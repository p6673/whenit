<?php

namespace App\Http\Controllers;

use App\Models\VoteModel;
use App\Models\Whenit;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class WhenitController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public function index()
    {
        return view('whenit', ['todayEvent' => Whenit::where('account', session('id'))->whereRaw('DATE(summary) = "' . $this->today() . '"')->get(), 'uncompleted' => Whenit::where('account', session('id'))->where('status', false)->orderBy('created_at')->get(), 'archived' => Whenit::where('account', session('id'))->where('status', 2)->orderBy('created_at')->get(), 'completed' => Whenit::where('account', session('id'))->where('status', true)->orderBy('summary')->get(), 'today' => $this->today()]);
    }

    public function store(Request $request)
    {
        $whenit = new Whenit;
        $whenit->id = $this->randString(64);
        $whenit->account = session('id');
        $whenit->title = $request->title;
        $whenit->start = $request->start;
        $whenit->end = $request->end;
        $whenit->desc = $request->desc;

        $whenit->save();
        return redirect('/');
    }

    public function update(Request $request, Whenit $whenit)
    {
        $validate = $request->validate([
            'title' => "required",
            'start' => "required",
            'end' => "required",
            'desc' => ""
        ]);
        $whenit->update($validate);
        return redirect('/');
    }

    public function archive(Whenit $whenit)
    {
        $whenit->update(['status' => 2]);
        return redirect('/');
    }

    public function unarchive(Whenit $whenit)
    {
        $whenit->update(['status' => 1]);
        return redirect('/');
    }

    public function destroy(Whenit $whenit)
    {
        VoteModel::where('event', $whenit->id)->delete();
        $whenit->delete();
        return redirect('/');
    }
}
