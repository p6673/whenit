<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Whenit extends Model
{
    use HasFactory;
    protected $table = 'whenit';
    protected $guarded = [];
    public $incrementing = false;
}
