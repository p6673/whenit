<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>WhenIt</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body class="bg0">
    <main class="container py-5 ps-5 pe-0">
        <div class="d-flex justify-content-between">
            <h1 class="f800 fc1 mb-5 ms-2 ps-5">When It</h1>
            <div class="d-flex px-4 r40 py-3 text-center bg1 align-self-center mb-5">
                <p class="ps-2 fc1 f700 fs-6 m-0 align-self-center">{{ session('name') }}</p>
                <img src="{{ session('picture') }}" class="rounded-circle mx-3" style="width: 2.5rem" alt="">
                <a class="px-2 align-self-center bi bi-box-arrow-right btn3 fs-4" href="/logout"
                    onclick="return confirm('Are you sure to Logout?')"></a>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <x-card :shadow="true" style="primary">
                    <h2 class="ps-4 py-2 mb-4 fs-4 fc1 f800">New Event</h2>
                    <form action="/" method="POST">
                        @csrf
                        <input type="text" style="outline:none"
                            class="border-0 r20 bg0 px-4 py-3 me-4 fs-5 fc1 f700 w-100" placeholder="Title"
                            name="title">
                        <div class="d-flex mt-4">
                            <span class="f700 fc2 fs-6 align-self-center px-3">From</span>
                            <input type="date" class="f700 bg0 border-0 r20 fc1 px-4 py-3 w-100" placeholder="Start"
                                name="start">
                            <span class="f700 fc2 fs-6 align-self-center px-3">to</span>
                            <input type="date" class="f700 bg0 border-0 r20 fc1 px-4 py-3 w-100" placeholder="End"
                                name="end">
                        </div>
                        <textarea class="border-0 bg0 r20 my-4 p-4 fs-6 fc1 f700 w-100" style="outline:none" rows="3" placeholder="Description"
                            name="desc"></textarea>
                        <div class="d-flex mb-3">
                            <button type="submit" class="f700 shdw r20 btn1 px-5 py-3">Add Event</button>
                            <button type="reset" class="f700 r20 btn3 px-4 py-3 ">Clear</button>
                        </div>
                    </form>
                </x-card>
            </div>
            <div class="col-6 ps-5">
                <h2 class="ps-4 py-2 mb-4 fs-4 fc1 f800">Today's Events</h2>
                @forelse ($todayEvent as $ev)
                    <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                        <x-card class="me-3 mb-5" :shadow="true" style="primary">
                            <div class="px-3 py-4">
                                <div class="d-flex">
                                    <div class="align-self-center rounded-circle me-3"
                                        style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                    </div>
                                    <p class="align-self-center m-0 f800 fs-6 fc1">
                                        Vote Completed
                                    </p>
                                </div>
                                <div class="pt-3">
                                    <p class="fs-4 cl0 f700 mb-0">
                                        {{ $ev->title }}
                                    </p>
                                    <p class="fs-7 fc2 clamp3 f700 text">
                                        {{ $ev->desc }}
                                    </p>
                                    <div class="d-flex justify-content-end">
                                        <p class="mb-0 fs-6 cl0 f700 ">
                                            {{ $ev->summary }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </x-card>
                    </a>
                @empty
                    <x-card class="me-3 mb-5" :shadow="true" style="primary">
                        <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Events Today</h2>
                    </x-card>
                @endforelse
            </div>

            <div class="col-12 pt-4">
                <section class="pt-5 ">
                    <h2 class="ps-5 py-2 fs-4 fc1 f800">Event List</h2>
                    <section class="row py-5">
                        <p class="fc1 fs-6 f800 ps-5 mb-4 ms-2">Vote Completed</p>
                        @forelse ($completed as $ev)
                            <div class="col-4">
                                <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                                    <div class="p-5 r40 me-3 mb-4 shdw @if (explode(' ', $ev->summary)[0] == $today) bg1 @endif"
                                        @if (explode(' ', $ev->summary)[0] != $today) style="background:#dbeafe" @endif>
                                        <div class="d-flex">
                                            <div class="align-self-center rounded-circle me-3"
                                                style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                            </div>
                                            <p class="align-self-center m-0 f800 fs-7 fc1">
                                                Vote Completed
                                            </p>
                                        </div>
                                        <div class="pt-3">
                                            <p class="fs-5 cl0 f700 mb-0">
                                                {{ $ev->title }}
                                            </p>
                                            <p class="fs-7 fc2 f700 text clamp1">
                                                {{ $ev->desc }}
                                            </p>
                                            <div class="d-flex justify-content-end">
                                                <p
                                                    class="mb-0 fs-7 @if (explode(' ', $ev->summary)[0] == $today) cl0 @else fc1 @endif f700 ">
                                                    {{ $ev->summary }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @empty
                            <x-card class="me-3 mb-5" :shadow="true" style="primary">
                                <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Events</h2>
                            </x-card>
                        @endforelse
                    </section>

                    <section class="row py-5">
                        <p class="fc1 fs-6 f800 ps-5 mb-4 ms-2">Vote Uncompleted</p>
                        @forelse ($uncompleted as $ev)
                            <div class="col-4">
                                <div data-bs-toggle="modal" data-bs-target="#mainModal"
                                    onclick="setData({{ Illuminate\Support\Js::from($ev) }})">
                                    <div class="p-5 r40 me-3 mb-4 shdw" style="background:#fff1f2">
                                        <div class="d-flex">
                                            <div class="align-self-center rounded-circle me-3"
                                                style="width:1rem; height:1rem; border:3px solid var(--cl1)">
                                            </div>
                                            <p class="align-self-center m-0 f800 fs-7 fc1">
                                                Vote Uncompleted
                                            </p>
                                        </div>
                                        <div class="pt-3">
                                            <p class="fs-5 cl1 f700 mb-0">
                                                {{ $ev->title }}
                                            </p>
                                            <p class="fs-7 fc2 f700 text clamp1">
                                                {{ $ev->desc }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <x-card class="me-3 mb-5" :shadow="true" style="primary">
                                <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Events</h2>
                            </x-card>
                        @endforelse
                    </section>

                    <section class="row py-5">
                        <p class="fc1 fs-6 f800 ps-5 mb-4 ms-2">Archived Events</p>
                        @forelse ($archived as $ev)
                            <div class="col-4">
                                <a href="/vote/{{ $ev->id }}" class="text-decoration-none">
                                    <x-card class="me-3 mb-5" :shadow="true" style="primary">
                                        <div class="p-3">
                                            <div class="d-flex">
                                                <div class="align-self-center rounded-circle me-3"
                                                    style="width:1rem; height:1rem; border:3px solid var(--cl0)">
                                                </div>
                                                <p class="align-self-center m-0 f800 fs-7 fc2">
                                                    Vote Completed
                                                </p>
                                            </div>
                                            <div class="pt-3">
                                                <p class="fs-6 fc2 f700 mb-0">
                                                    {{ $ev->title }}
                                                </p>
                                                <p class="fs-7 fc2 clamp3 f700 text">
                                                    {{ $ev->desc }}
                                                </p>
                                                <div class="d-flex justify-content-end">
                                                    <p class="mb-0 fs-7 fc2 f700 ">
                                                        {{ $ev->summary }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </x-card>
                                </a>
                            </div>
                        @empty
                            <x-card class="me-3 mb-5" :shadow="true" style="primary">
                                <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Archives</h2>
                            </x-card>
                        @endforelse
                    </section>
                </section>
            </div>
        </div>
    </main>


    <div class="modal fade" id="mainModal" tabindex="-1" aria-labelledby="mainModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="background:transparent;">
                <x-card :shadow="true" style="primary">
                    <div class="d-flex justify-content-between mb-4">
                        <div class="d-flex">
                            <h5 class="ps-4 py-2 fs-5 fc1 f700 modal-title">Event Detail</h5>
                            <button type="button" class="btn cl0"
                                onclick="return navigator.clipboard.writeText(linkShare)"><i
                                    class="bi bi-clipboard"></i></button>
                        </div>
                        <form action="/destroy" id="deleteForm" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="align-self-center btn m-0 p-2 cl1 f700 fs-6"
                                onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                        </form>
                    </div>
                    <form action="/" id="updateForm" method="POST">
                        @csrf
                        @method('PATCH')
                        <input type="text" style="outline:none"
                            class="border-0 r20 bg0 px-4 py-3 me-4 fs-5 fc1 f700 w-100" placeholder="Title" name="title"
                            id="vTitle">
                        <div class="d-flex mt-4">
                            <span class="f700 fc2 fs-6 align-self-center px-3">From</span>
                            <input type="date" class="f700 bg0 border-0 r20 fc1 px-4 py-3 w-100" placeholder="Start"
                                name="start" id="vStart">
                            <span class="f700 fc2 fs-6 align-self-center px-3">to</span>
                            <input type="date" class="f700 bg0 border-0 r20 fc1 px-4 py-3 w-100" placeholder="End"
                                name="end" id="vEnd">
                        </div>
                        <textarea class="border-0 bg0 r20 my-4 p-4 fs-6 fc1  f700 w-100" style="outline:none" rows="3" placeholder="Description"
                            name="desc" id="vDesc"></textarea>
                        <div class="d-flex mb-4">
                            <a href="/vote/" id="voteDetail" class="f700 align-self-center w-100 bg0 py-3 r20 btn2">Vote
                                Detail</a>
                        </div>
                        <div>
                            <button type="submit" class="f700 r20 btn1 px-5 py-3 ">Update
                                Event</button>
                            <button type="button" class="f700 r20 btn3 px-4 py-3"
                                data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </x-card>
            </div>
        </div>
    </div>
    <script>
        let linkShare = 'http://127.0.0.1:8000/vote';
        const setData = (data) => {
            linkShare = 'http://127.0.0.1:8000/vote/' + data.id + '/form';
            $('#vTitle').val(data.title);
            $('#vStatus').val(data.status);
            $('#vStart').val(data.start);
            $('#vEnd').val(data.end);
            $('#vSummary').val(data.summary);
            $('#vDesc').val(data.desc);
            $("#updateForm").attr('action', '/' + data.id);
            $("#deleteForm").attr('action', '/' + data.id);
            $("#voteDetail").attr('href', '/vote/' + data.id);
        }
    </script>
</body>

</html>
