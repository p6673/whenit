<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Vote</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body class="bg0">
    <div class="container my-5 s">
        <div class="d-flex justify-content-between py-5 mb-5">
            <div class="align-self-center mb-3">
                <a href="/" class="btn2 ps-3 pe-4 py-2 r20"><span class="bi bi-chevron-left">&nbsp; back to
                        WhenIt</span></a>
            </div>
            <div class="d-flex justify-content-center align-self-center">
                <h1 class="f700 fc1 text-center">Vote Dashboard</h1>
                @if ($id->status)
                    <span class="cl0 bg1 py-2 px-3 r20 fs-7 mb-2 align-self-center"
                        style="letter-spacing: 0.04em;">completed</span>
                @else
                    <span class="cl1 bg1 py-2 px-3 r20 fs-7 mb-2 align-self-center"
                        style="letter-spacing: 0.04em;">uncompleted</span>
                @endif
            </div>
            <div class="@if ($id->status)  @endif align-self-center mb-3 d-flex">
                @if (!$id->status)
                    <a href="/vote/{{ $id->id }}/form" target="blank"
                        class="btn1 me-3 py-2 px-4 r20 f700">Form</a>
                @else
                    <form action="/{{ $id->id }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="align-self-center btn m-0 p-2 cl1 f700 fs-6"
                            onclick="return confirm('Are you sure to delete this event ?')">Delete</button>
                    </form>
                    @if ($id->status == 2)
                        <form action="/{{ $id->id }}/unarchive" method="POST">
                            @method('PATCH')
                            @csrf
                            <button type="submit" class="align-self-center btn1 m-0 py-2 px-3 f700 fs-6 r20 ms-3"
                                onclick="return confirm('Are you sure to unarchive this event ?')">Unarchive</button>
                        </form>
                    @else
                        <form action="/{{ $id->id }}/archive" method="POST">
                            @method('PATCH')
                            @csrf
                            <button type="submit" class="align-self-center btn1 m-0 py-2 px-3 f700 fs-6 r20 ms-3"
                                onclick="return confirm('Are you sure to archive this event ?')">Archive</button>
                        </form>
                    @endif
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-6 pe-5">
                <div class="mx-3 px-3 mb-5">
                    <h1 class="fs-5 fc1 f700">Event : {{ $id->title }}</h1>
                    <h6 class="fs-7 fc2 f700">Description : {{ $id->desc }}</h6>
                    <h6 class="fs-7 fc2 f700">Start From <span class="fc1">{{ $id->start }}</span> to
                        <span class="fc1">{{ $id->end }}</span>
                    </h6>
                </div>
                @forelse ($event as $ev)
                    @if ($id->status && date('Y-m-d', strtotime($id->summary)) == $ev->vote)
                        <x-card :shadow="true" style="primary" class="mb-4">
                            <div class="d-flex justify-content-between">
                                <p class="f700 cl0 fs-5 m-0">
                                    {{ $ev->vote }}</p>
                                <p class="f700 fc2 fs7 m-0">{{ $ev->voted }} participans</p>
                            </div>
                            <p class="f700 fc2 fs-7">Avg Duration : {{ round($ev->dur, 2) }} hours</p>
                            <div class="d-flex justify-content-between">
                                <p class="cl0 fs-5 m-0">
                                    {{ $summary[$ev->vote]['start'] . ' to ' . $summary[$ev->vote]['end'] }} </p>
                                <span class="m-0 py-2 r20 px-4 f700"
                                    style="background-color: #3b82f6; color: var(--bg0);">Selected</span>
                            </div>
                        </x-card>
                        @continue
                    @endif
                    <x-card :shadow="true" style="primary" class="mb-4">
                        <div class="d-flex justify-content-between">
                            <p class="f800 @if ($id->status) fc2 @else cl0 @endif fs-6 m-0">
                                {{ $ev->vote }}</p>
                            <p class="f700 fc2 fs7 m-0">{{ $ev->voted }} participans</p>
                        </div>
                        <p class="f700 fc2 fs-7">Avg Duration : {{ round($ev->dur, 2) }} hours</p>
                        <div class="d-flex justify-content-between">
                            <p class="@if ($id->status) fc2 @else fc1 @endif fs-5 m-0">
                                {{ $summary[$ev->vote]['start'] . ' to ' . $summary[$ev->vote]['end'] }} </p>
                            @if (!$id->status)
                                <a href="/vote/{{ $id->id }}/summary/{{ $ev->vote }}/{{ $summary[$ev->vote]['start'] }}"
                                    class="btn1 m-0 py-2 r20 px-3 f700">Select</a>
                            @endif
                        </div>
                    </x-card>
                @empty
                    <x-card class="me-3 mb-5" :shadow="true" style="primary">
                        <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Vote</h2>
                    </x-card>
                @endforelse
            </div>
            <div class="col-6 ps-5">
                <p class="f700 fc1 fs-5">Participans <span
                        class="bg1 py-1 px-3 r20 fs-6 align-self-center">{{ count($participans) }}</span> </p>
                @forelse ($participans as $p)
                    <div class="d-flex justify-content-between py-3">
                        <div class="w-100">
                            <div class="d-flex justify-content-between">
                                <p class="f700 fc1 fs-6 clamp1 w-75">{{ $p->name }}</p>
                                <p class="f700 fc2 fs-6">{{ round($p->total, 2) }} hours</p>
                            </div>
                            <x-progress color="#3b82f6" :percentage="($p->total / $max) * 100"></x-progress>
                        </div>
                        @if (!$id->status)
                            <a href="/vote/{{ $id->id }}/hide/{{ $p->inputId }}"
                                class="f700 fs-6 align-self-center btn1 px-3 py-2 ms-4 r20">hide</a>
                        @endif
                    </div>
                @empty
                    <x-card class="me-3 mb-5" :shadow="true" style="primary">
                        <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Participans</h2>
                    </x-card>
                @endforelse

                <p class="f700 fc1 fs-6 mt-5">Hided Participans <span
                        class="bg1 py-1 px-3 r20">{{ count($hided) }}</span></p>
                @forelse ($hided as $hd)
                    <div class="d-flex justify-content-between pb-3">
                        <div class="w-100">
                            <div class="d-flex justify-content-between">
                                <p class="f700 fc1 fs-6">{{ $hd->name }}</p>
                                <p class="f700 fc2 fs-6">{{ round($hd->total, 2) }} hours</p>
                            </div>
                            <x-progress color="#94a3b8" :percentage="($hd->total / $max) * 100"></x-progress>
                        </div>
                        @if (!$id->status)
                            <a href="/vote/{{ $id->id }}/show/{{ $hd->inputId }}"
                                class="f700 fs-6 align-self-center px-2 py-1 btn2 ms-4 r20">show</a>
                        @endif
                    </div>
                @empty
                    <x-card class="me-3 mb-5" :shadow="true" style="primary">
                        <h2 class="ps-4 py-5 text-center m-0 fs-5 fc2 f700">No Hided Participans</h2>
                    </x-card>
                @endforelse
            </div>
        </div>
    </div>
</body>

</html>
