<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Vote Form</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body class="bg0">
    <div class="m-auto py-5" style="width: 50rem">
        <x-card :shadow="true" style="primary">
            <div class="p-4">
                @if ($id->status)
                    <h1 class="f700 fc1 pb-5 text-center">Vote Completed</h1>
                    <h1 class="fs-5 fc1 f700">{{ $id->title }}</h1>
                    <h6 class="fs-7 fc2 f700">This vote is about <i>{{ $id->desc }}</i></h6>
                    <h6 class="fs-7 fc2 f700">The summary date is <span
                            class="fc1">{{ $id->summary }}</span></h6>
                @else
                    <h1 class="f700 fc1 pb-5 text-center">WhenIt</h1>
                    <h1 class="fs-5 fc1 f700">{{ $id->title }}</h1>
                    <h6 class="fs-7 fc2 f700">This vote is about <i>{{ $id->desc }}</i></h6>
                    <h6 class="fs-7 fc2 f700">Start From <span class="fc1">{{ $id->start }}</span> to
                        <span class="fc1">{{ $id->end }}</span>
                    </h6>
                    <br>
                    <br>
                    <form action="http://127.0.0.1:8000/vote/{{ $id->id }}/form" method="POST">
                        @csrf
                        <div class="r20 d-flex bg0 p-3 shdw mb-5">
                            <label for="inName" class="align-self-center px-3 fs-6 fc1 f700">Name</label>
                            <input type="text" id="inName" style="outline:none"
                                class="border-0 bg-transparent fs-5 fc1 f700 w-100" name="name">
                        </div>
                        @for ($a = 0; $a < count($id->summary); $a++)
                            <x-card :shadow="false" style="secondary" class="my-4">
                                <button type="button" class="fs-6 btn2 f700 py-1 px-2 r20" id="sum{{ $a }}"
                                    onclick="allDay({{ $a }})">{{ $id->summary[$a] }}</button>
                                <span class="fs-6 fc2 f700 ps-5">From</span>
                                <input type="time" class="f700 bg0 border-0 r20 fc2 px-4 py-3 w-auto"
                                    name="start{{ $a }}" id="start{{ $a }}"
                                    onchange="swap({{ $a }})" placeholder="time Start">
                                <span class="fs-6 fc2 f700">To</span>
                                <input type="time" class="f700 bg0 border-0 r20 fc2 px-4 py-3 w-auto"
                                    name="end{{ $a }}" id="end{{ $a }}"
                                    onchange="swap({{ $a }})" placeholder="time End">
                                <span class="fs-6 fc2 f700" id="clr{{ $a }}" style="cursor:pointer"
                                    onclick="clr({{ $a }})">Clear</span>
                                <br>
                            </x-card>
                        @endfor
                        <br>
                        <button type="submit" class="f700 btn1 r20 w-100 py-3">Submit</button>
                    </form>
                @endif
            </div>
        </x-card>
    </div>
    <script>
        const allDay = (id) => {
            console.log(id);
            $('#start' + id).val('00:01')
            $('#end' + id).val('23:59')
            swap(id)
        }
        const swap = (id) => {
            if ($('#start' + id).val() != "" && $('#end' + id).val() != "") {
                $('#start' + id).removeClass('fc2');
                $('#start' + id).addClass('cl0');
                $('#end' + id).removeClass('fc2');
                $('#end' + id).addClass('cl0');
                $('#clr' + id).removeClass('fc2');
                $('#clr' + id).addClass('cl1');
                if ($('#start' + id).val() > $('#end' + id).val()) {
                    let temp = $('#start' + id).val();
                    $('#start' + id).val($('#end' + id).val())
                    $('#end' + id).val(temp)
                }
            } else {
                $('#start' + id).removeClass('cl0');
                $('#start' + id).addClass('fc2');
                $('#end' + id).removeClass('cl0');
                $('#end' + id).addClass('fc2');
                $('#clr' + id).removeClass('cl1');
                $('#clr' + id).addClass('fc2');
            }
        }
        const clr = (id) => {
            $('#start' + id).prop('disabled', true);
            $('#start' + id).val('');
            $('#start' + id).prop('disabled', false);
            $('#end' + id).prop('disabled', true);
            $('#end' + id).val('');
            $('#end' + id).prop('disabled', false);
            swap(id);
        }
    </script>
</body>

</html>
