<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}"></script>
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>

<body class="bg0">
    <div style="width: 35rem" class="m-auto my-5 py-5">
        <x-card :shadow="true" style="primary" class="text-center pb-5">
            <div class="px-3 pt-3 pb-5 mb-5">
                <h2 class="f700 fc1">When It</h2>
                <h6 class="f700 fc2 mb-5">Welcome Back!</h6>
                <a href="auth/google" class="btn bg0 fc1 f700 r40 w-75 p-2 fs-5"><img
                        src="{{ asset('icon/google.png') }}" style="width:2.5rem;" alt="">&emsp;
                    Login with Google</a>
            </div>
        </x-card>
    </div>
</body>

</html>
