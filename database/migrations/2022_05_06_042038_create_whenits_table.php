<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhenitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whenit', function (Blueprint $table) {
            $table->string('id')->nullable(false);
            $table->string('account')->nullable(false);
            $table->string('title')->nullable(true);
            $table->text('desc')->nullable(true);
            $table->date('start')->nullable(false);
            $table->date('end')->nullable(false);
            $table->dateTime('summary')->nullable(true);
            $table->boolean('status')->nullable(false)->default(false);
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whenits');
    }
}
