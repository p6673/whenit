<?php

use App\Http\Controllers\GoogleController;
use App\Http\Controllers\VoteController;
use App\Http\Controllers\WhenitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('guest')->group(function () {
    Route::get('/login', [GoogleController::class, 'index']);
    Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
    Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
});
Route::get('/vote/{id}/form', [VoteController::class, "form"]);
Route::post('/vote/{id}/form', [VoteController::class, "store"]);

Route::middleware('login')->group(function () {
    Route::get('/', [WhenitController::class, 'index']);
    Route::post('/', [WhenitController::class, 'store']);
    Route::patch('/{whenit}', [WhenitController::class, 'update']);
    Route::patch('/{whenit}/archive', [WhenitController::class, 'archive']);
    Route::patch('/{whenit}/unarchive', [WhenitController::class, 'unarchive']);
    Route::delete('/{whenit}', [WhenitController::class, 'destroy']);

    Route::get('/vote/{id}', [VoteController::class, "index"]);
    Route::get('/vote/{id}/summary', [VoteController::class, "summary"]);
    Route::get('/vote/{id}/summary/{date}/{time}', [VoteController::class, "setSummary"]);
    Route::get('/vote/{id}/hide/{hide}', [VoteController::class, "hide"]);
    Route::get('/vote/{id}/show/{show}', [VoteController::class, "show"]);
    Route::get('/logout', function () {
        session()->flush();
        return redirect('/login');
    });
});
